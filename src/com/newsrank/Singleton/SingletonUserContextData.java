package com.newsrank.Singleton;

import java.util.Date;

import android.R.string;
import android.util.Log;

import com.newsrank.AsycTasks.DownloadList;
import com.newsrank.FragmentModels.RegistrationDataPacket;
import com.newsrank.response.xml.parsing.Handlers.LoginResponseData;
import com.newsrank.AsycTasks.SendRankingData;
// Should probably figure out what the max data to be returned from a login and a registration so we can populate all these fields.
public class SingletonUserContextData
{
	private String apiKey;

	private String firstName;
	private String lastName;
	private String email;
	
	private long timeTaken;
	private long startTime;
	private long endTime;
	private String articleGUID;
	

	static SingletonUserContextData instance = new SingletonUserContextData();

	public static SingletonUserContextData getInstance()
	{
		return instance;
	}


	public void setApiKey(LoginResponseData objectContainingApiKey)
	{
		this.apiKey = objectContainingApiKey.getApiKey();
	}

	public void setUserData(RegistrationDataPacket objectContainingUserData)
	{
		this.firstName = objectContainingUserData.getFirstName();
		this.lastName = objectContainingUserData.getLastName();
		this.email = objectContainingUserData.getEmail();
	}

	public void setUserData(String firstName, String lastName, String email)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public String getApiKey()
	{
		return apiKey;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getEmail()
	{
		return email;
	}
	
	public void startTiming(String currentArticleGUID)
	{
		startTime = System.currentTimeMillis();
		Log.d("TIME","StartTiming:"+startTime);
	}
	
	public void endTiming()
	{
		endTime = startTime - System.currentTimeMillis();
		Log.d("TIME","EndTiming:"+endTime);
	}
	
	public void sendData()
	{
		new SendRankingData(timeTaken,articleGUID);
	}

}
