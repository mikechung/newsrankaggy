package com.newsrank.Singleton;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.os.Environment;

import com.newsrank.response.xml.parsing.Handlers.LoginResponseData;
import com.newsrank.response.xml.parsing.Handlers.LoginResponseHandler;

public class SingletonNetworkController
{
	// Url Paths
	private String baseURLImage = "http://aggy.hopto.org";
	private String baseURL = "http://aggy.hopto.org/django/";
	private String apiURL =  "api/v1/";
	private String user = "user/";
	private String requestKeyURL = "reqkey/";
	private String currentArticleURL = "article/";
	private String signUpURL = "signup/";
	private String responseFormatXml = "?format=xml";
	private String emailField = "&email=";
	private String passField = "&password=";
	private String firstField = "&first=";
	private String lastField = "&last=";
	
	// Android home paths
	private String newsRankImagePath = Environment.getExternalStorageDirectory() + "/Android/data/com.NewsRank/cache/Images/";
	private String newsRankHomeStoragePath = Environment.getExternalStorageDirectory() + "/Android/data/com.NewsRank/cache/";
	private String newsRankHomeXmlPath =  "xml/";
	private String newsRankRSSXmlPath = newsRankHomeStoragePath + newsRankHomeXmlPath + "RSSNews.xml";
	/**
	 * "http://aggy.hopto.org:2300/api/v1/article/?format=xml"
	 * @return
	 */
	
	public String getImageBasePathStorage()
	{
		return newsRankImagePath;
	}
	
	public String getImageBasePath()
	{
		return baseURLImage;
	}
	public String getXmlStoragePath()
	{
		return newsRankHomeStoragePath + newsRankHomeXmlPath;
	}
	
	public String getNewsRankXmlFile()
	{
		return newsRankRSSXmlPath;
	}
	
	public String httpGetArticleXml()
	{
		return baseURL + apiURL + currentArticleURL + responseFormatXml;
	}
	
	/**
	 * TODO Should be a post method
	 * @return
	 */
	public String httpPostLoginURL(String email,String password)
	{
		return baseURL + apiURL + user + requestKeyURL + responseFormatXml + emailField + email + passField + password;
	}
	
	/**
	 * TODO Should be a post method
	 * @return
	 */
	public String httpPostRegistrationURL(String email,String password,String first,String last)
	{
		return  baseURL + apiURL + user + signUpURL+ responseFormatXml + emailField + email + passField + password + firstField + first + lastField + last;
	}
	
	/**
	 *  TODO Friend Add  Friend Request Confirm and Obtain Friends List
	 */
	
	
	private static SingletonNetworkController instance = new SingletonNetworkController();

	public static SingletonNetworkController getInstance()
	{
		return instance;
	}

	
	public LoginResponseData loginToAccount(String email, String password)
	{
		LoginResponseData data;
		
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet("http://aggy.hopto.org:2300/api/v1/user/reqkey/?format=xml&email=" + email + "&password=" + password);
		try
		{
			// Read the input from the response xml
			HttpResponse response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();

			SAXParserFactory saxPF = SAXParserFactory.newInstance();

			SAXParser saxP = null;
			try
			{
				saxP = saxPF.newSAXParser();
			}
			catch (ParserConfigurationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			XMLReader xmlR = null;
			try
			{
				xmlR = saxP.getXMLReader();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Use my custom xml handler for all xml handling
			LoginResponseHandler loginResponseHandler = new LoginResponseHandler();

			xmlR.setContentHandler(loginResponseHandler);
			try
			{
				xmlR.parse(new InputSource(entity.getContent()));
			}
			catch (IllegalStateException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			data = loginResponseHandler.getLoginResponseData();

			// This means it failed to register because the name is already in
			// use.

			// else return true and tell about registration success

			return data;

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Fail anyways since the try didn't at all work
		return null;

	}

	
	
	
}

