package com.newsrank.FragmentViews;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.newsrank.R;
import com.newsrank.Adapters.ArticleAdapter;
import com.newsrank.AsycTasks.DownloadList;
import com.newsrank.AsycTasks.LoadUrlAsycTask;
import com.newsrank.FragmentModels.ArticleDataResponsePacket;
import com.newsrank.FragmentViewListeners.ArticleListListener;
import com.newsrank.FragmentViewListeners.RSSFeedListViewListener;
import com.newsrank.Singleton.SingletonUserContextData;

/*
 * This class contains the RSS Feed list and view that will allow navigation of the list.
 * It is controlled by the  RSS Feed Fragment Controller.
 * You must set the view data using the Adapter.
 * You must also set the listeners for the view.
 */

// Where going use FrameLayout instead of relative layout
public class RSSFeedFragmentView extends FrameLayout
{

	protected static final String ERROR = null;
	private ListView rssFeedListView;
	private RSSFeedListViewListener rssListViewListener;
    private ArticleListListener articleListListener;
    
    
    public void setArticleListListener(ArticleListListener articleViewListener)
    {
    	this.articleListListener = articleViewListener;
    }
    
	public RSSFeedFragmentView(Context context)
	{
		super(context);
	}

	// Might need this?
	public RSSFeedFragmentView(Context context, AttributeSet attrs)
	{
		// Call the RelativeLayout Params
		super(context, attrs);
	}

	protected void onFinishInflate()
	{
		// Must call this otherwise error?
		super.onFinishInflate();
		rssFeedListView = (ListView) findViewById(R.id.RSSListView);
		rssFeedListView.setClickable(true);
		// Start downloading the stuff.

		// TODO Remove this is to test the application.

		rssFeedListView.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{

			   Object o = rssFeedListView.getItemAtPosition(position);
			   ArticleDataResponsePacket article = (ArticleDataResponsePacket)o; 
			   
			  

			   new LoadUrlAsycTask(articleListListener,article).execute();
			   
			    //articleListListener.sendWebViewUrl(article.getURL());
			    Log.e(ERROR, "I AM CLICKING ON AN ARTICLE YAY~~~~~");
			}
		});

	}

	/**
	 * This will define what will be displayed in each row. The layout for the
	 * row will be defined in a layout.
	 * 
	 * @param adapterForRSSListView
	 */
	public void setAdapterForRSSListView(ArticleAdapter adapterForRSSListView)
	{
		rssFeedListView.setAdapter(adapterForRSSListView);
	}

	public ArticleAdapter getAdapterForRSSListView()
	{
		return (ArticleAdapter) rssFeedListView.getAdapter();
	}

	public void setRSSListViewListener(RSSFeedListViewListener rssViewListener)
	{
		this.rssListViewListener = rssViewListener;
	}

	public ListView getRSSListView()
	{
		return rssFeedListView;
	}

}
