package com.newsrank.FragmentViews;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.newsrank.R;
import com.newsrank.Adapters.ArticleAdapter;
import com.newsrank.Adapters.StaggeredArticleAdapter;
import com.newsrank.AsycTasks.DownloadList;
import com.newsrank.FragmentViewListeners.RSSFeedListViewListener;
import com.origamilabs.library.views.StaggeredGridView;

/*
 * This class contains the RSS Feed list and view that will allow navigation of the list.
 * It is controlled by the  RSS Feed Fragment Controller.
 * You must set the view data using the Adapter.
 * You must also set the listeners for the view.
 */

// Where going use FrameLayout instead of relative layout
public class RSSStaggeredFragmentView extends FrameLayout
{

	private StaggeredGridView rssFeedGridView;
	private RSSFeedListViewListener rssListViewListener;

	public RSSStaggeredFragmentView(Context context)
	{
		super(context);
	}
	
	// Might need this?
	public RSSStaggeredFragmentView(Context context, AttributeSet attrs)
	{
		// Call the RelativeLayout Params
		super(context, attrs);
	}

	protected void onFinishInflate()
	{
		// Must call this otherwise error?
		super.onFinishInflate();
		rssFeedGridView = (StaggeredGridView) findViewById(R.id.RSSStaggeredGridView);

		// Start downloading the stuff.
		
		

	}

	/**
	 * This will define what will be displayed in each row. The layout for the
	 * row will be defined in a layout.
	 * 
	 * @param adapterForRSSListView
	 */
	public void setAdapterForRSSListView(StaggeredArticleAdapter adapterForRSSListView)
	{
		rssFeedGridView.setAdapter(adapterForRSSListView);
	}
	
	public StaggeredArticleAdapter getAdapterForRSSListView()
	{
		return (StaggeredArticleAdapter)rssFeedGridView.getAdapter();
	}

	public void setRSSListViewListener(RSSFeedListViewListener rssViewListener)
	{
		this.rssListViewListener = rssViewListener;
	}
	
	public StaggeredGridView getRSSListView()
	{
		return rssFeedGridView;
	}

}