package com.newsrank.FragmentViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.newsrank.R;
import com.newsrank.FragmentViewListeners.RSSFeedListViewListener;

/*
 * This class contains the RSS Feed list and view that will allow navigation of the list.
 * It is controlled by the  RSS Feed Fragment Controller.
 * You must set the view data using the Adapter.
 * You must also set the listeners for the view.
 */

// Where going use FrameLayout instead of relative layout
public class RSSFeedPlaceHolderView extends FrameLayout
{

	private ListView rssFeedListView;
	private RSSFeedListViewListener rssListViewListener;

	public RSSFeedPlaceHolderView(Context context)
	{
		super(context);
	}
	

	public RSSFeedPlaceHolderView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	protected void onFinishInflate()
	{
		// Must call this otherwise error?
		super.onFinishInflate();
		rssFeedListView = (ListView) findViewById(R.id.RSSListView);

		// TODO Remove this is to test the application.
		
		rssFeedListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3)
			{
				// Delegate to controller to open up a web view or the display
				// where the story will be read.
			}

		});

	}

	/**
	 * This will define what will be displayed in each row. The layout for the
	 * row will be defined in a layout.
	 * 
	 * @param adapterForRSSListView
	 */
	public void setAdapterForRSSListView(ArrayAdapter adapterForRSSListView)
	{
		rssFeedListView.setAdapter(adapterForRSSListView);
	}

	public void setRSSListViewListener(RSSFeedListViewListener rssViewListener)
	{
		this.rssListViewListener = rssViewListener;
	}

}
