package com.newsrank.FragmentViews;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.newsrank.R;

/*
 * This class contains the RSS Feed list and view that will allow navigation of the list.
 * It is controlled by the  RSS Feed Fragment Controller.
 * You must set the view data using the Adapter.
 * You must also set the listeners for the view.
 */

// Where going use FrameLayout instead of relative layout
public class WebFragmentView extends FrameLayout
{
	WebView webView;

	public WebFragmentView(Context context)
	{
		super(context);
	}

	public WebFragmentView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	protected void onFinishInflate()
	{
		super.onFinishInflate();

		webView = (WebView) findViewById(R.id.WebView);

		webView.setWebViewClient(new CustomWebViewClient());
		webView.getSettings().setJavaScriptEnabled(true);

	}

	public void LoadUrl(String url)
	{
		webView.loadUrl(url);
	}

	private class CustomWebViewClient extends WebViewClient
	{
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
		{
			view.loadUrl(url);
			return true;
		}
	}

}
