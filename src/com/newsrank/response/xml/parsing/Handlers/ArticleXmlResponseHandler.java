package com.newsrank.response.xml.parsing.Handlers;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.newsrank.FragmentModels.ArticleDataResponsePacket;

public class ArticleXmlResponseHandler extends DefaultHandler
{
	private boolean object, objects,category, description, pubdate, title, image, link, id, resource_uri,next,previous,rank;
	
	private String previouslistUrl;
	private String nextListUrl;
	
	private ArticleDataResponsePacket articleData;
	
	public String getPreviousURL()
	{
		return previouslistUrl;
	}
	
	public String getNextUrl()
	{
		return nextListUrl;
	}
	
	public ArrayList<ArticleDataResponsePacket> getArticleList()
	{
		// Updated Article list.
		return articleList;
	}
	
	ArrayList<ArticleDataResponsePacket> articleList;
	
	public ArticleXmlResponseHandler()
	{
		
	}

	public void startDocument() throws SAXException
	{
		// Create a new article list to pass information into.
		articleList = new ArrayList<ArticleDataResponsePacket>();
	}

	public void endDocument() throws SAXException
	{

	}

	/**
	 * This gets called at the start of an element. Here we're also setting the
	 * booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{

		if (localName.equals("objects"))
		{
			
			objects = true;
		}

		if (localName.equals("object"))
		{
			articleData = new ArticleDataResponsePacket(null, null, null, null,null);
			object = true;
		}
		
		if(localName.equals("image"))
		{
			image = true;
		}
	
		else if (localName.equals("category"))
		{
			category = true;
		}

		else if (localName.equals("pubdate"))
		{
			pubdate = true;
		}

		else if (localName.equals("title"))
		{
			title = true;
		}

		else if (localName.equals("link"))
		{
			link = true;
		}

		else if (localName.equals("id"))
		{
			id = true;
		}

		else if (localName.equals("resource_uri"))
		{
			resource_uri = true;
		}
		
		else if(localName.equals("next"))
		{
			next = true;
		}
		
		else if(localName.equals("previous"))
		{
			previous = true;
		}
		
		else if(localName.equals("rank"))
		{
			rank = true;
		}
	}

	/**
	 * Called at the end of the element. Setting the booleans to false, so we
	 * know that we've just left that tag.
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @throws SAXException
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	{
		

		if (localName.equals("objects"))
		{
			objects = false;
		}

		else if (localName.equals("category"))
		{
			category = false;
		}

		else if (localName.equals("pubdate"))
		{
			pubdate = false;
		}

		else if (localName.equals("title"))
		{
			title = false;
		}

		else if (localName.equals("link"))
		{
			link = false;
		}

		else if (localName.equals("id"))
		{
			id = false;
		}
		
		else if (localName.equals("image"))
		{
			image = false;
		}

		else if (localName.equals("resource_uri"))
		{
			resource_uri = false;
		}
		
		else if (localName.equals("object"))
		{
			// End of the first object
			articleList.add(articleData);
			object = false;	
		}
		
		// The end of the XML file
		else if (localName.equals("next"))
		{
			next = false;
		}
		
		else if (localName.equals("previous"))
		{
			previous = false;
		}
		
		else if (localName.equals("rank"))
		{
			rank = false;
		}

	}

	/**
	 * Calling when we're within an element. Here we're checking to see if there
	 * is any content in the tags that we're interested in and populating it in
	 * the Config object.
	 * 
	 * @param ch
	 * @param start
	 * @param length
	 */
	@Override
	public void characters(char ch[], int start, int length)
	{
		String chars = new String(ch, start, length);
		chars = chars.trim();
		

		if (category)
		{
			articleData.setCategory(chars.toString());
		}

		else if (pubdate)
		{
			
		}

		else if (title)
		{
			articleData.setTitle(chars.toString());
		}

		else if (id)
		{
			articleData.setGUID(chars.toString());
		}

		else if (link)
		{
			articleData.setURL(chars.toString());
		}

		else if (resource_uri)
		{
			//articleData.
		}
		
		else if (next)
		{
			nextListUrl = chars.toString();
		}
		
		else if(image)
		{
			articleData.setImageURL(chars.toString());
		}
		
		else if(previous)
		{
			previouslistUrl = chars.toString();
		}
		
		else if(rank)
		{
			//rank = chars.toString();
		}

	}

}
