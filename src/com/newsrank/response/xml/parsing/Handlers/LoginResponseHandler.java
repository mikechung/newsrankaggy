package com.newsrank.response.xml.parsing.Handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;


public class LoginResponseHandler extends DefaultHandler
{
	private boolean inApiKey,inFailure,inName;

	private LoginResponseData registrationKeyData;

	public LoginResponseData getLoginResponseData()
	{
		return registrationKeyData;
	}

	public void startDocument() throws SAXException
	{
		registrationKeyData = new LoginResponseData();
	}

	public void endDocument() throws SAXException
	{

	}

	/**
	 * This gets called at the start of an element. Here we're also setting the
	 * booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{

		if(localName.equals("APIKey"))
		{
			inApiKey = true;
		}
		
		else if (localName.equals("failed"))
		{
			inFailure = true;
		}
		else if (localName.equals("Name"))
		{
			inName = true;
		}

	}

	/**
	 * Called at the end of the element. Setting the booleans to false, so we
	 * know that we've just left that tag.
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @throws SAXException
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	{
		Log.v("endElement", localName);

		if(localName.equals("APIKey"))
		{
			inApiKey = true;
		}
		
		else if (localName.equals("failed"))
		{
			inFailure = true;
		}
		else if (localName.equals("Name"))
		{
			inName = true;
		}
	}
	
	/**
	 * Calling when we're within an element. Here we're checking to see if there
	 * is any content in the tags that we're interested in and populating it in
	 * the Config object.
	 * 
	 * @param ch
	 * @param start
	 * @param length
	 */
	@Override
	public void characters(char ch[], int start, int length)
	{
		String chars = new String(ch, start, length);
		chars = chars.trim();

		 if (inApiKey)
		{
			registrationKeyData.setApiKey(chars.toString());
		}
		
		else if (inFailure)
		{
			registrationKeyData.setStatus(false);
		}
		
		else if (inName)
		{
			registrationKeyData.setName(chars.toString());
		}
		
		
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
