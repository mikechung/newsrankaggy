package com.newsrank.response.xml.parsing.Handlers;

public class AccountRegistrationResponseData
{
	int throttled;
	int created;
	int inUse;
	
	public void setAccountCreationData(AccountRegistrationResponseData rhs)
	{
		this.created = rhs.getCreated();
		this.inUse = rhs.getInUse();
		this.throttled = rhs.getThrottled();
	}
	
	public int getThrottled()
	{
		return throttled;
	}
	
	public void setThrottled(int throttled)
	{
		this.throttled = throttled;
	}
	
	public int getCreated()
	{
		return created;
	}
	
	public void setCreated(int created)
	{
		this.created = created;
	}
	
	public int getInUse()
	{
		return inUse;	
	}
	
	public void setInUse(int inUse)
	{
		this.inUse = inUse;
	}
	
	
	
	
}
