package com.newsrank.response.xml.parsing.Handlers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


import android.util.Log;


public class AccountCreateResponseHandler extends DefaultHandler
{
	private boolean inCreated, inUse, inThrottled;

	private AccountRegistrationResponseData accountRegistrationData;

	public AccountRegistrationResponseData getAccountCreationData()
	{
		return accountRegistrationData;
	}

	public void startDocument() throws SAXException
	{
		accountRegistrationData = new AccountRegistrationResponseData();
	}

	public void endDocument() throws SAXException
	{

	}

	/**
	 * This gets called at the start of an element. Here we're also setting the
	 * booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{

		if (localName.equals("throttled"))
		{
			inThrottled = true;
		}
		else if (localName.equals("created"))
		{
			inCreated = true;
		}
		else if (localName.equals("in_use"))
		{
			inUse = true;
		}

	}

	/**
	 * Called at the end of the element. Setting the booleans to false, so we
	 * know that we've just left that tag.
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @throws SAXException
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	{
		Log.v("endElement", localName);

		if (localName.equals("throttled"))
		{
			inThrottled = false;
		}
		else if (localName.equals("created"))
		{
			inCreated = false;
		}
		else if (localName.equals("in_use"))
		{
			inUse = false;
		}

	}
	
	/**
	 * Calling when we're within an element. Here we're checking to see if there
	 * is any content in the tags that we're interested in and populating it in
	 * the Config object.
	 * 
	 * @param ch
	 * @param start
	 * @param length
	 */
	@Override
	public void characters(char ch[], int start, int length)
	{
		String chars = new String(ch, start, length);
		chars = chars.trim();

		if (inThrottled)
		{
			accountRegistrationData.setThrottled(Integer.parseInt(chars.toString()));
		}
		else if (inCreated)
		{
			accountRegistrationData.setCreated(Integer.parseInt(chars.toString()));
		}
		else if (inUse)
		{
			accountRegistrationData.setInUse(Integer.parseInt(chars.toString()));
		}
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}