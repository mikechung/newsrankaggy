package com.newsrank.FragmentControllers;

import com.newsrank.FragmentViewListeners.RSSFeedListViewListener;
import com.newsrank.FragmentViews.RSSFeedPlaceHolderView;
import com.newsrank.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * This class is the controller for the RSSFeedView, which contains the list.
 * This will be attached to a Fragment Activity.
 * This is a placeholder for future code development.
 */
public class RSSFeedFragmentPlaceHolderController extends Fragment
{
	// Create the model.
	ArrayAdapter<String> adapterForRssFeedView;
	// Testing values to try and check if the view works.
	String[] values = new String[]
	{ "Android", "iPhone", "WindowsMobile", "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X", "Linux", "OS/2" };

	// Testing values to try and check if the view works.

	// TODO Create the model that will feed the list view.
	// Create the view.
	private RSSFeedPlaceHolderView rssFeedView;
	// Create the view listener.
	private RSSFeedListViewListener rssFeedViewListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the one view for the RSSFeedView
		rssFeedView = (RSSFeedPlaceHolderView) inflater.inflate(R.layout.custom_rss_feed_view_layout_placeholder, container, false);

		// Initialize the adapter
		adapterForRssFeedView = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, values);

		rssFeedView.setAdapterForRSSListView(adapterForRssFeedView);

		// Attach the listener for the view create the private anon class.
		rssFeedViewListener = new RSSFeedListViewListener();
		rssFeedView.setRSSListViewListener(rssFeedViewListener);
		// Attach the Adapter after the test is done

		// Create the controller class.
		return rssFeedView;
	}

	// Create private controller delegate class with the listener here.

}
