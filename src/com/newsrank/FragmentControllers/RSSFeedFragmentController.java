package com.newsrank.FragmentControllers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.net.ConnectivityManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.newsrank.R;
import com.newsrank.Adapters.ArticleAdapter;
import com.newsrank.AsycTasks.DownloadList;
import com.newsrank.FragmentViewListeners.ArticleListListener;
import com.newsrank.FragmentViewListeners.RSSFeedListViewListener;
import com.newsrank.FragmentViews.RSSFeedFragmentView;
import com.newsrank.Singleton.SingletonNetworkController;
import com.newsrank.Singleton.SingletonUserContextData;

/**
 * This class is the controller for the RSSFeedView, which contains the list.
 * This will be attached to a Fragment Activity.
 */
public class RSSFeedFragmentController extends Fragment
{

	ArticleAdapter adapterForRssFeedView;

	// Testing values to try and check if the view works.

	// Testing values to try and check if the view works.

	// TODO Create the model that will feed the list view.
	// Create the view.
	private RSSFeedFragmentView rssFeedView;
	// Create the view listener.
	private RSSFeedListViewListener rssFeedViewListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		// Inflate the one view for the RSSFeedView
		rssFeedView = (RSSFeedFragmentView) inflater.inflate(R.layout.custom_rss_feed_view_layout, container, false);

		// Initialize the adapter
		adapterForRssFeedView = new ArticleAdapter(getActivity().getBaseContext());

		// Set the adapter
		rssFeedView.setAdapterForRSSListView(adapterForRssFeedView);

		// Set the list listener
		rssFeedView.setArticleListListener(articleListListener);

		// Attach the listener for the view create the private anon class.
		rssFeedViewListener = new RSSFeedListViewListener();

		rssFeedView.setRSSListViewListener(rssFeedViewListener);
		// Attach the Adapter after the test is done

		// Create the controller class.
		DownloadList();

		return rssFeedView;
	}

	public void DownloadList()
	{
		if (CheckNetwork.isInternetAvailable(RSSFeedFragmentController.this.getActivity()))
		{
			new DownloadList(rssFeedView.getRSSListView(), rssFeedView.getAdapterForRSSListView()).execute(SingletonNetworkController.getInstance().httpGetArticleXml());
		}
		else
		{
			Toast.makeText(RSSFeedFragmentController.this.getActivity(), "No Internet Connection", 1000).show();
		}
	}

	private static class CheckNetwork
	{
		private static final String TAG = CheckNetwork.class.getSimpleName();

		public static boolean isInternetAvailable(Context context)
		{
			NetworkInfo info = (NetworkInfo) ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

			if (info == null)
			{
				Log.d(TAG, "no internet connection");
				return false;
			}
			else
			{
				if (info.isConnected())
				{
					Log.d(TAG, " internet connection available...");
					return true;
				}
				else
				{
					Log.d(TAG, " internet connection");
					return true;
				}

			}
		}
	}

	private ArticleListListener articleListListener = new ArticleListListener()
	{

		@Override
		public void sendWebViewUrl(String url, String guid)
		{
		
			

			if (CheckNetwork.isInternetAvailable(RSSFeedFragmentController.this.getActivity()))
			{
				BaseSliderFragmentController base = (BaseSliderFragmentController) RSSFeedFragmentController.this.getActivity();
				base.getSupportActionBar().setSelectedNavigationItem(1);
				WebViewFragmentController toDelegateTo = (WebViewFragmentController) RSSFeedFragmentController.this.getActivity().getSupportFragmentManager().findFragmentByTag("Web");
				Log.i("Sending the Request to the Web Fragment", url);
				toDelegateTo.getWebView().LoadUrl(url);	
				SingletonUserContextData.getInstance().startTiming(guid);
				
			}
			else
			{
				Toast.makeText(RSSFeedFragmentController.this.getActivity(), "No Internet Connection", 1000).show();
			}

		}

	};

	// Create private controller delegate class with the listener here.

}
