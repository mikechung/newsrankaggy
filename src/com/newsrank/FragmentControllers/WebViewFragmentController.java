package com.newsrank.FragmentControllers;
import java.util.Date;

import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.newsrank.R;
import com.newsrank.FragmentViews.WebFragmentView;

public class WebViewFragmentController extends Fragment
{
	
	
	private WebFragmentView webView;
	
	public WebFragmentView getWebView()
	{
		return webView;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		
		webView = (WebFragmentView) inflater.inflate(R.layout.custom_webview, container, false);	
		return webView;
	}
	

}
