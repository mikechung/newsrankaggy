package com.newsrank.FragmentControllers;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.newsrank.R;
import com.newsrank.Adapters.ArticleAdapter;
import com.newsrank.Adapters.StaggeredArticleAdapter;
import com.newsrank.AsycTasks.DownloadList;
import com.newsrank.AsycTasks.DownloadStaggeredList;
import com.newsrank.FragmentViewListeners.RSSFeedListViewListener;
import com.newsrank.FragmentViews.RSSFeedFragmentView;
import com.newsrank.FragmentViews.RSSStaggeredFragmentView;
import com.newsrank.Singleton.SingletonNetworkController;

public class RSSStaggeredFragmentController extends Fragment
{


StaggeredArticleAdapter adapterForRssFeedView;


// Testing values to try and check if the view works.


// Testing values to try and check if the view works.

// TODO Create the model that will feed the list view.
// Create the view.
private RSSStaggeredFragmentView rssStaggeredFeedView;

// Create the view listener.
private RSSFeedListViewListener rssFeedViewListener;

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
{

	// Inflate the one view for the RSSFeedView
	rssStaggeredFeedView = (RSSStaggeredFragmentView) inflater.inflate(R.layout.custom_rss_feed_view_staggered_layout, container, false);

	// Initialize the adapter
	adapterForRssFeedView = new StaggeredArticleAdapter(getActivity().getBaseContext());

	// Set the adapter
	rssStaggeredFeedView.setAdapterForRSSListView(adapterForRssFeedView);

	// Attach the listener for the view create the private anon class.
	rssFeedViewListener = new RSSFeedListViewListener();
	
	rssStaggeredFeedView.setRSSListViewListener(rssFeedViewListener);
	// Attach the Adapter after the test is done

	// Create the controller class.
	

	new DownloadStaggeredList(rssStaggeredFeedView.getRSSListView(),rssStaggeredFeedView.getAdapterForRSSListView()).execute(SingletonNetworkController.getInstance().httpGetArticleXml());
	
	
	return rssStaggeredFeedView;
}



// Create private controller delegate class with the listener here.

}