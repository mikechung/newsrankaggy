package com.newsrank.FragmentControllers;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.newsrank.R;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;


public class BaseSliderFragmentController extends SlidingFragmentActivity
{

	private int mTitleRes;
	protected RSSFeedFragmentPlaceHolderController mFrag;

	public BaseSliderFragmentController(int titleRes)
	{
		mTitleRes = titleRes;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setTitle(mTitleRes);

		// This is the Left menu to be replaced with login or registration.
		setBehindContentView(R.layout.dummy_left_slider_fragment_layout);
		FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
		mFrag = new RSSFeedFragmentPlaceHolderController();
		t.replace(R.id.dummy_left, mFrag);
		t.commit();

		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case android.R.id.home:
			toggle();
			return true;

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		return true;
	}

	

}
