	package com.newsrank.FragmentControllers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.newsrank.R;
import com.slidingmenu.lib.SlidingMenu;
import com.newsrank.FragmentControllers.*;
import com.newsrank.Singleton.SingletonUserContextData;

/**
 * This class contains the Centre View that will be responsible for the tabs
 * that will navigate between the tabs.
 * 
 * @author michaelchung
 */
public class LeftRightSliderFragmentController extends BaseSliderFragmentController
{
	
    RSSFeedFragmentController toDelegateTo;
    
	public LeftRightSliderFragmentController()
	{
		super(R.string.app_name);
	}
	
	@Override
	  public boolean onCreateOptionsMenu(Menu menu) 
	{
	    MenuInflater inflater = getSupportMenuInflater();
	    inflater.inflate(R.layout.custom_actionbar_sherlock_menu, menu);
	    
	    return true;
	  } 
	
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode == KeyEvent.KEYCODE_BACK) {
	     //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
	     return true;
	     }
	     return super.onKeyDown(keyCode, event);    
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
		    switch (item.getItemId()) {
		    case R.id.action_refresh:
		    	toDelegateTo = (RSSFeedFragmentController)getSupportFragmentManager().findFragmentByTag("RSS");
		    	toDelegateTo.DownloadList();
		    	// Switch to the right tab.
		    	getSupportActionBar().setSelectedNavigationItem(0);
		    	
		      break;

		    default:
		      break;
		    }

		    return true;
		  } 
	
	
	

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getSlidingMenu().setMode(SlidingMenu.LEFT_RIGHT);
		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

		
		/**
		 * We don't need the Centre Fragment since we actually are going to use the action bar
		 * With Fragment switching.
		 */
		// This is actually the center menu actually
		// Replaces the Center or Base Dummy with the RSSFeedFragment.
		// We are going to use the Fragment to be overlayed
		setContentView(R.layout.dummy_base_slider_fragment_layout);
		
		// Replace the id
		// getSupportFragmentManager().beginTransaction().replace(R.id.dummy_left,
		// new RSSFeedFragmentPlaceHolderController()).commit();

		// Replaces the Right Dummy with the RSSFeedFragment
		getSlidingMenu().setSecondaryMenu(R.layout.dummy_right_slider_fragment_layout);
		getSlidingMenu().setSecondaryShadowDrawable(R.drawable.shadowright);
		// Replace the id
		
		// This is where the friends list view will go.
		getSupportFragmentManager().beginTransaction().replace(R.id.dummy_right, new RSSFeedFragmentPlaceHolderController()).commit();

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		ActionBar actionBar = getSupportActionBar();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.show();
		
		
		//Create instances of the fragments here then.....
		//Pass them into tab listener
		
		Fragment newsFragment = Fragment.instantiate(this, RSSFeedFragmentController.class.getName());
		
		Fragment webViewFragment = Fragment.instantiate(this,WebViewFragmentController.class.getName());
		
		Tab newsFragmentTab = getSupportActionBar().newTab().setText("News").setTabListener(new StateHeldTabListener(newsFragment,"RSS"));
		
		Tab webViewFragmentTab = getSupportActionBar().newTab().setText("View Story").setTabListener(new StateHeldTabListener(webViewFragment,"Web"));
		
		
		// Cheap hack to get the stupid call back to register the fragment for the tabs..... stupid android api.
		getSupportActionBar().addTab(newsFragmentTab,true);
		getSupportActionBar().setSelectedNavigationItem(0);
		getSupportActionBar().addTab(webViewFragmentTab,true);
		getSupportActionBar().setSelectedNavigationItem(1);
		getSupportActionBar().setSelectedNavigationItem(0);


	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onStop()
	{
		super.onStop();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	public class StateHeldTabListener implements TabListener
	{
		private static final String ERROR = "ERROR";
		private Fragment mFragment;
		private final String mTag;
		boolean mAdded;
		
		// Prevent singleton from crashing.
		boolean firstTime;
		public StateHeldTabListener(Fragment fragment,String tag)
		{
			this.mFragment = fragment;
			this.mTag = tag;
			this.mAdded = false;
			this.firstTime = false;
			
		
       
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft)
		{
			// Uses the Fragment Transaction from an mysterious source in which I cannot add it to here.
			if (mAdded == false)
			{
				
				// If not, instantiate and add it to the activity
				ft.add(R.id.dummy_base, mFragment, mTag);
				mAdded = true;
			}
			
			if(tab.getText().toString().equals("View Story"))
			{
			   LeftRightSliderFragmentController.this.getSlidingMenu().setSlidingEnabled(false);
			}
			
			if(tab.getText().toString().equals("News"))
			{
				LeftRightSliderFragmentController.this.getSlidingMenu().setSlidingEnabled(true);
				SingletonUserContextData.getInstance().endTiming();
			}
			
			
			ft.show(mFragment);
			
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft)
		{
			ft.hide(mFragment);		
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft)
		{
			ft.show(mFragment);			
		}
	
		
	}
	
}
