package com.newsrank.FragmentModels;

public class ArticleDataResponsePacket
{
	private String title;
	private String category;
	private String imageURL;
	private String URL;
	private String GUID;

	public ArticleDataResponsePacket(final String title, final String URL, final String imageURL, final String GUID,final String category)
	{
		this.title = title;
		this.category = category;
		this.URL = URL;
		this.imageURL = imageURL;
		this.GUID = GUID;
	}
	
	public void setCategory(String category)
	{
		this.category = category;
	}
	
	public String getCategory()
	{
		return category;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void setImageURL(String imageUrl)
	{
		this.imageURL = imageUrl;
	}
	
	public void setURL(String url)
	{
		this.URL = url;
	}
	
	public void setGUID(String guid)
	{
		this.GUID = guid;
	}
	
	/**
	 * @return Title of news entry
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @return image of news entry
	 */
	public String getImageURL()
	{
		return imageURL;
	}

	/**
	 * @return URL of news entry
	 */
	public String getURL()
	{
		return URL;
	}

	/**
	 * @return GUID of news entry
	 */
	public String getGUID()
	{
		return GUID;
	}

}
