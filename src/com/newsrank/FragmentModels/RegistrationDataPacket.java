package com.newsrank.FragmentModels;

public class RegistrationDataPacket
{

	private String emailAddress;
	private String userName;
	private String lastName;
	private String password;

	public void setRegistrationData(RegistrationDataPacket rhs)
	{
		this.emailAddress = rhs.getEmail();
		this.password = rhs.getPassword();
		this.userName = rhs.getFirstName();
		this.lastName = rhs.getLastName();
	}

	public void setEmail(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public void setFirstName(String userName)
	{
		this.userName = userName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getEmail()
	{
		return this.emailAddress;
	}

	public String getFirstName()
	{
		return this.userName;
	}

	public String getLastName()
	{
		return this.lastName;
	}

	public String getPassword()
	{
		return this.password;
	}
}
