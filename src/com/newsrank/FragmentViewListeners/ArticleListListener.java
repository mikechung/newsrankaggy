package com.newsrank.FragmentViewListeners;

import android.webkit.WebView;

public interface  ArticleListListener 
{
	public void sendWebViewUrl(String url,String guid);
}
