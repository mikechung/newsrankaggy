package com.newsrank.FragmentViewListeners;

import android.content.Context;
import android.widget.Toast;

public class ListViewListener
{
	public void NotifyUser(String string,Context context)
	{
		CharSequence text = string;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}
}
