package com.newsrank.Adapters;

import java.io.File;

import com.newsrank.R;
import com.newsrank.Adapters.ArticleAdapter.ViewHolder;
import com.newsrank.FragmentModels.ArticleDataResponsePacket;
import com.newsrank.Singleton.SingletonNetworkController;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewDebug.IntToString;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class StaggeredArticleAdapter extends ArrayAdapter<ArticleDataResponsePacket>
{
	
	public StaggeredArticleAdapter(Context context)
	{
		super(context, R.layout.custom_article_adapter_row_layout);
		// TODO Auto-generated constructor stub
	}

	static class ViewHolder
	{
		public TextView title;
		// Add later for the staggered grid view.
		public TextView position;
		public ImageView image;
	}

	/**
	 * This is the method that the ListView would use to obtain the row and from
	 * the adapter and have it formatted the way the
	 * custom_article_adapter_row_layout wants it to be.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup Parent)
	{
		View rowView = convertView;	
		
		if (rowView == null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = inflater.inflate(R.layout.custom_article_adapter_staggered_row_layout, null);
			ViewHolder viewHolder = new ViewHolder();
	
			viewHolder.title = (TextView) rowView.findViewById(R.id.article_title);
			viewHolder.position = (TextView) rowView.findViewById(R.id.article_position);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.article_image);
			rowView.setTag(viewHolder);
		}

		// Grabs the article at the from position number
		ArticleDataResponsePacket article = getItem(position);
		ViewHolder holder = (ViewHolder) rowView.getTag();
		// Set the title for the holder
		holder.title.setText(article.getTitle());
		holder.position.setText(Integer.toString((position)));
		
		// File of where the image is
		File image = new File(SingletonNetworkController.getInstance().getImageBasePathStorage()+ article.getGUID());
		
		if(!image.exists())
		{
		holder.image.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.no_image));
		}
		else
		{
		// Image place
		holder.image.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
		}

		return rowView;
	}

}
