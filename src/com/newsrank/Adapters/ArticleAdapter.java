package com.newsrank.Adapters;

import java.io.File;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.newsrank.R;
import com.newsrank.FragmentModels.ArticleDataResponsePacket;
import com.newsrank.Singleton.SingletonNetworkController;


/**Article Adapter
 * The Article Adapter is a Array Adapter that uses the Article DataResponsePacket
 * and formats it for use with a view that uses an ArrayAdapter.In this case the view 
 * it is to be used with is a List View.
 * @author Michael Chung
 */
public class ArticleAdapter extends ArrayAdapter<ArticleDataResponsePacket>
{
	
	public ArticleAdapter(Context context)
	{
		//** The file that will be used to reference the article adapter. */
		super(context, R.layout.custom_article_adapter_row_layout);
	}

	/**
	 * This View Holder is used to cache the objects below. 
	 */
	static class ViewHolder
	{
		public TextView title;
		public TextView position;
		public ImageView image;
	}

	/**
	 * This is the method that the ListView would use to obtain the row and from
	 * the adapter and have it formatted the way the  custom_article_adapter_row_layout 
	 * wants it to be.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup Parent)
	{
		View rowView = convertView;

		if (rowView == null)
		{
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			rowView = inflater.inflate(R.layout.custom_article_adapter_row_layout, null);
			ViewHolder viewHolder = new ViewHolder();

			viewHolder.title = (TextView) rowView.findViewById(R.id.article_title);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.article_image);
			viewHolder.position = (TextView) rowView.findViewById(R.id.article_position);
		
			rowView.setTag(viewHolder);
		}

		ArticleDataResponsePacket article = getItem(position);
		ViewHolder holder = (ViewHolder) rowView.getTag();
		
		holder.title.setText(article.getTitle());
		holder.position.setText(Integer.toString(position));

		File image = new File(SingletonNetworkController.getInstance().getImageBasePathStorage() + article.getGUID());

		if (!image.exists())
		{
			holder.image.setImageBitmap(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.no_image));
		}
		else
		{
			holder.image.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
		}

		return rowView;
	}

}
