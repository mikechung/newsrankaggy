package com.newsrank.AsycTasks;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.os.AsyncTask;

import com.newsrank.FragmentModels.RegistrationDataPacket;
import com.newsrank.Singleton.SingletonNetworkController;
import com.newsrank.response.xml.parsing.Handlers.AccountCreateResponseHandler;
import com.newsrank.response.xml.parsing.Handlers.AccountRegistrationResponseData;



public class CreateAccount extends AsyncTask<Void, Void, Void>
{
	
	private RegistrationDataPacket registrationDataPacket;
	private AccountRegistrationResponseData accountRegistrationReponseData;
	
	CreateAccount(RegistrationDataPacket registrationDataPacket)
	{
		this.registrationDataPacket = registrationDataPacket;
	}
	
	@Override
	protected Void doInBackground(Void... params)
	{
		DefaultHttpClient httpclient = new DefaultHttpClient();
		
		HttpGet httpGet = new HttpGet(SingletonNetworkController.getInstance().httpPostRegistrationURL(registrationDataPacket.getEmail(), registrationDataPacket.getPassword(),registrationDataPacket.getFirstName(),registrationDataPacket.getLastName()));
		// Read the input from the response.
		HttpResponse response = null;
		try
		{
			response = httpclient.execute(httpGet);
		}
		catch (ClientProtocolException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		HttpEntity entity = response.getEntity();

		SAXParserFactory saxPF = SAXParserFactory.newInstance();

		SAXParser saxP = null;
		try
		{
			saxP = saxPF.newSAXParser();
		}
		catch (ParserConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		XMLReader xmlR = null;
		try
		{
			xmlR = saxP.getXMLReader();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		AccountCreateResponseHandler accountCreateResponseHandler = new AccountCreateResponseHandler();

		xmlR.setContentHandler(accountCreateResponseHandler);
		try
		{
			try
			{
				xmlR.parse(new InputSource(entity.getContent()));
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		catch (IllegalStateException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 accountRegistrationReponseData = accountCreateResponseHandler.getAccountCreationData();
		return null;
	}

	
	protected void onPostExecute(Void v)
	{
		// Provide key information for the user context
		// sure that the user understands that there is an action happening. with the spinning.
		//  Then create a asyc task that shows success of registration or not.
		
	}




}