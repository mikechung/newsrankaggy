package com.newsrank.AsycTasks;

import com.newsrank.FragmentModels.ArticleDataResponsePacket;
import com.newsrank.FragmentViewListeners.ArticleListListener;

import android.os.AsyncTask;
import android.util.Log;

public class LoadUrlAsycTask extends AsyncTask<Void, Void, Void>
{

	private ArticleListListener articleListListener;
	private ArticleDataResponsePacket articleUrl;
	
	/**
	 * Allow the thread to modify these two items.
	 * @param articleListListener 
	 * 
	 * @param articleListListener
	 */
	public LoadUrlAsycTask(ArticleListListener articleListListener,ArticleDataResponsePacket article)
	{
		this.articleListListener = articleListListener;
		this.articleUrl = article;
	}

	/**
	 * If you run Execute.(Pass in list of Generic Strings Parameters) Those
	 * will be passed to this function which will process them. Of Course here
	 * we are only using the single url that will return to us a list we pass in
	 * the url that is the current server list of articles.
	 */
	@Override
	protected Void doInBackground(Void... params)
	{
		return null;
	}
	
	@Override
	protected void onPostExecute(Void unused)
	{
		articleListListener.sendWebViewUrl(articleUrl.getURL(),articleUrl.getGUID());
		Log.i("LOADING URL", articleUrl.getURL());
	}


}

