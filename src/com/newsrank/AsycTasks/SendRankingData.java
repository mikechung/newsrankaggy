package com.newsrank.AsycTasks;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.newsrank.Singleton.SingletonUserContextData;

import android.os.AsyncTask;
import android.util.Log;

public class SendRankingData extends AsyncTask<String, Void, String>
{

	Long time;
	String id;

	public SendRankingData(Long time, String id)
	{
		this.time = time;
		this.id = id;
	}

	@Override
	protected String doInBackground(String... params)
	{
		String res = "";

		res = sendData();

		return res;
	}

	@Override
	protected void onPostExecute(String result)
	{
		Log.d("TIME","RANKING");
	}

	private String sendData()
	{

		String apiKey = SingletonUserContextData.getInstance().getApiKey();
		if (apiKey == null)
		{
			String email = SingletonUserContextData.getInstance().getEmail();
			try
			{
				//URL url = new URL("http://aggy.hopto.org:2300/api/v1/time/add/?username=" + email + "&api_key=" + apiKey + "&time=" + time + "&article=" + id + "&format=xml");
				URL url = new URL("http://aggy.hopto.org/django/api/v1/time/add/?username=" + email + "&api_key=" + apiKey + "&time=" + time + "&article=" + id + "&format=xml");
				System.out.println(url);

				// create the new connection
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				// set up some things on the connection
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(false);
				// and connect!
				urlConnection.connect();
				InputStream inputStream = urlConnection.getInputStream();
				inputStream.close();
				urlConnection.disconnect();
				return "Data sent Successfully";
			}
			catch (Exception e)
			{
				System.out.println(e);
				return "Data not sent";
			}
		}
		return "Not logged in";
	}
}
