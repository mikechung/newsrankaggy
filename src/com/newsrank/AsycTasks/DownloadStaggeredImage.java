package com.newsrank.AsycTasks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.newsrank.R;
import com.newsrank.Adapters.ArticleAdapter;
import com.newsrank.Adapters.StaggeredArticleAdapter;
import com.newsrank.FragmentModels.ArticleDataResponsePacket;
import com.newsrank.Singleton.SingletonNetworkController;

public class DownloadStaggeredImage extends AsyncTask<ArticleDataResponsePacket, Void, Void>
{

	StaggeredArticleAdapter articleAdapter;

	public DownloadStaggeredImage(StaggeredArticleAdapter articleAdapter)
	{
		this.articleAdapter = articleAdapter;
	}

	@Override
	protected Void doInBackground(ArticleDataResponsePacket... articleItems)
	{
		for (ArticleDataResponsePacket item : articleItems)
		{
			download(item);
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void v)
	{
		articleAdapter.notifyDataSetChanged();
	}

	private void download(ArticleDataResponsePacket currentArticleItem)
	{

		Bitmap bm = BitmapFactory.decodeResource(null, R.drawable.no_image);
		
		//TODO localize that string
		String imagePath = Environment.getExternalStorageDirectory() + "/Android/data/com.NewsRank/cache/Images/";
		Log.i("LOGG",imagePath);
		try
		{
			URL aURL = new URL(currentArticleItem.getImageURL());
			Log.i("LOGG",currentArticleItem.getImageURL());
			HttpURLConnection conn = (HttpURLConnection) aURL.openConnection();
			InputStream is = new BufferedInputStream(conn.getInputStream());
			
			/* Buffered is always good for a performance plus. */
			BufferedInputStream bis = new BufferedInputStream(is);

			bm = BitmapFactory.decodeStream(bis);

			bis.close();
			is.close();
			
			conn.disconnect();

			/** Should probably check before downloading.... **/
			
			// Checks to see if the path to store images exists, if not

			File fileImage = new File(imagePath);
			if (!fileImage.exists())
			{
				fileImage.mkdirs();
			}

			// saves image locally for use
			FileOutputStream fos = new FileOutputStream(imagePath + currentArticleItem.getGUID(), false);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			bm.compress(CompressFormat.JPEG, 100, bos);

			bos.flush();
			bos.close();
		} catch (IOException e)
		{
			Log.e("DEBUGTAG", "Remote Image Exception", e);
		}

	}

}