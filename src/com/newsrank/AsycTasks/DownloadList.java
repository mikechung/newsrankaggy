package com.newsrank.AsycTasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

import com.newsrank.Adapters.ArticleAdapter;
import com.newsrank.FragmentModels.ArticleDataResponsePacket;
import com.newsrank.Singleton.SingletonNetworkController;
import com.newsrank.response.xml.parsing.Handlers.AccountRegistrationResponseData;
import com.newsrank.response.xml.parsing.Handlers.ArticleXmlResponseHandler;

public class DownloadList extends AsyncTask<String, Void, String>
{

	ListView listView;
	ArticleAdapter articleAdapter;
	ListView newFeedAdapter;

	private StringBuilder inputStreamToString(InputStream is)
	{
		String line = "";
		StringBuilder total = new StringBuilder();

		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));

		// Read response until the end
		try
		{
			while ((line = rd.readLine()) != null)
			{
				total.append(line);
				// Append new line as well for xml
				total.append("\n");
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Return full string
		return total;
	}

	/**
	 * Allow the thread to modify these two items.
	 * 
	 * @param listView
	 * @param articleAdapter
	 */
	public DownloadList(ListView listView, ArticleAdapter articleAdapter)
	{
		this.listView = listView;
		this.articleAdapter = articleAdapter;
	}

	/**
	 * If you run Execute.(Pass in list of Generic Strings Parameters) Those
	 * will be passed to this function which will process them. Of Course here
	 * we are only using the single url that will return to us a list we pass in
	 * the url that is the current server list of articles.
	 */
	@Override
	protected String doInBackground(String... urls)
	{
		String result = null;
		for (String url : urls)
		{
			result = downloadList(url);
		}
		return result;
	}

	protected void onPostExecute(String result)
	{
	
		SAXParserFactory saxPF = SAXParserFactory.newInstance();

		SAXParser saxP = null;

		try
		{
			saxP = saxPF.newSAXParser();
		}
		catch (ParserConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		XMLReader xmlR = null;
		try
		{
			xmlR = saxP.getXMLReader();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ArticleXmlResponseHandler articleXmlResponseHandler = new ArticleXmlResponseHandler();
		xmlR.setContentHandler(articleXmlResponseHandler);
		File rssXmlFile = new File(SingletonNetworkController.getInstance().getNewsRankXmlFile());
		
		FileReader fileReader = null;
		try
		{
			fileReader = new FileReader(rssXmlFile);
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BufferedReader bufferedReader= new BufferedReader(fileReader);
		InputSource inputSource = new InputSource(bufferedReader);
		
		
		try
		{
			xmlR.parse(inputSource);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Should have the articles now.	
		ArrayList<ArticleDataResponsePacket> newArticles = articleXmlResponseHandler.getArticleList();
		
		// Clear the article Adapter and refresh the list
		articleAdapter.clear();
		articleAdapter.addAll(newArticles);
		articleAdapter.notifyDataSetChanged();
		
		
//		//Print out the article information logged and formatted.
//		for(int i = 0; i< newArticles.size();i++)
//		{
//			Log.i("ARTICLES", "--------------------------------------------");
//			Log.i("ARTICLES", "Article Number: " + i );
//			 Log.i("ARTICLES", "Category:" + newArticles.get(i).getCategory());
//			 Log.i("ARTICLES", "Title:" + newArticles.get(i).getTitle());
//			 Log.i("ARTICLES", "URL:" + newArticles.get(i).getURL());
//			 Log.i("ARTICLES", "GUID:" + newArticles.get(i).getGUID());
//			 Log.i("ARTICLES","Image URL:" +newArticles.get(i).getImageURL());
//			 Log.i("ARTICLES","--------------------------------------------");
//		}
		
		
		for(int i = 0;i < newArticles.size();i++)
		{
			new DownloadImage(articleAdapter).execute(newArticles.get(i));
		}
		
		
		
	}

	private String downloadList(String xmlURL)
	{
		// Create an http connection then download this properly

		AccountRegistrationResponseData accountRegistrationResponseData;
		DefaultHttpClient httpclient = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(SingletonNetworkController.getInstance().httpGetArticleXml());

		HttpResponse response = null;
		try
		{
			response = httpclient.execute(httpGet);
		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (response != null)
		{
		HttpEntity entity = response.getEntity();
		}
		else
		{
			return "Failed To Get Server Response";
		}
		
		// Save to the file here
		File SDCardRoot = new File(SingletonNetworkController.getInstance().getXmlStoragePath());

		if (!SDCardRoot.exists())
		{
			SDCardRoot.mkdirs();
		}

		File rssNewsFile = new File(SDCardRoot, "RSSNews.xml");
		
		// If file exists don't create it again
		try
		{
			if(!rssNewsFile.exists())
			{
			rssNewsFile.createNewFile();
			}
		}
		catch (IOException e3)
		{
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		FileWriter fileWriter = null;
		try
		{
			fileWriter = new FileWriter(rssNewsFile);
		}
		catch (IOException e2)
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		StringBuilder rssXmlFile = null;
		try
		{
			rssXmlFile = inputStreamToString(response.getEntity().getContent());
			
		}
		catch (IllegalStateException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try
		{
			fileWriter.write(rssXmlFile.toString());
			
			//Log.d("ERROR", rssXmlFile.toString());
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try
		{
			fileWriter.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return xmlURL;

	}



}
